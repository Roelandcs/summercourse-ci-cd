# Chapter 1: Continuous Integration

Continuous Integration means that we are checking the validity of your code every time we push code to the repository.
In this chapter we are going to implement this by executing unit tests on our frontend and backend. 

This project contains a basic two-tier web application. 
The backend is contained in the `backend` folder and uses Spring Boot.
The Frontend is located in the `frontend` folder, which is written in Angular.

## Step 1: Testing our Frontend
First, we are going to execute our frontend tests! First, let's execute them locally (if possible):

```bash
cd frontend
npm install
npm test
```

Often you can run your CI-scripts locally, to test them. In this step you can use the commands above!

### Assignment
Define a job which uses the above CI script to verify the integrity of the frontend
*Hint: Use the `morlack/node-chromium` image. See README.md*

### Resources
- https://docs.gitlab.com/ee/ci/yaml/#script

## Step 2: Updating our Frontend
The frontend shows a name on the homepage. We should update this to be your name.

The code responsible for this is located in `frontend/src/app/app.component.ts`.

```typescript
export class AppComponent {
  title = 'summercourse-ci-cd';
}
```


### Assignment
Update the file shown above to your own title. Commit and watch the pipeline.

*Note: Your pipeline will fail. When it does, go to the next step*

## Step 3: Fix the tests
In the previous step we made our pipeline fail because we updated a title attribute, which made a test fail.

The tests failing is located in `frontend/src/app/app.component.spec.ts`:

```typescript
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('summercourse-ci-cd');   // <- should be your name!
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to summercourse-ci-cd!'); // <- should be Welcome to <your name>!
  }));
```

### Assignment
Fix the testcases above

## Step 4: Testing our Backend
We also want to test our backend. We can do this using the following commands:

```bash
cd backend
mvn test
```
### Assignment
Define a job which uses the above CI script to verify the integrity of the backend

*Hint: Use the `morlack/maven-8-aws-cli-docker` image. See README.md*
 

## End of Chapter
You have successfully made it through the first chapter! You can compare your results with the `results/chapter-1` branch now!
